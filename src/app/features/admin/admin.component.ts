import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../core/auth/auth.service';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ew-admin',
  template: `
    <p>
      admin works! {{authService.auth?.token}}
    </p>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(public authService: AuthService, private http: HttpClient) {
    http.get('http://localhost:3000/items')
      .subscribe(
        res => console.log(res),
        err => console.log('err', err)
      )
  }

  ngOnInit(): void {
  }

}
