import { ChangeDetectionStrategy, Component } from '@angular/core';
import { UsersService } from './services/users.service';
import { NavigationEnd, Router } from '@angular/router';

@Component({
  selector: 'ew-users',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    
    <ew-user-form
      (save)="usersService.addUser({})"
    ></ew-user-form>

    <ew-list
      [data]="usersService.users"
      (deleteUser)="usersService.deleteUser($event)"
    > </ew-list>
    <input type="text">
    <button routerLink="/users">users</button>
  `,
  styles: [
  ]
})
export class UsersComponent  {

  constructor(public usersService: UsersService, private router: Router) {
  /*
  this.router.routeReuseStrategy.shouldReuseRoute =  () => {
    return false;
  };
  */
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        console.log('api')
      }
    })
  }
}
