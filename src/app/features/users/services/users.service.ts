import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class UsersService {
  users: any[] = [{ id: 1, name: 'A'}, {id: 2, name: 'B'}, {id: 3, name: 'C'}]

  addUser(formData) {
    this.users = [...this.users, {}];
    // this.users.push({})
  }

  deleteUser(id: number) {
    // api call
   /* const index = this.users.findIndex(u => u.id === id );
    this.users.splice(index, 1);*/
    this.users = this.users.filter(u => u.id !== id );
  }

  editUser(formData) {
    this.users = this.users.map(u => {
      if (formData.id === u.id) {
        return {...u, ...formData};
      }
      return u;
    });
  }
}

