import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ew-list',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
     <ew-list-item 
       *ngFor="let user of data"
       [user]="user"
       (deleteUser)="deleteUser.emit($event)"
     ></ew-list-item>
  `
})
export class ListComponent  {
  @Input() data: any[] = [];
  @Output() deleteUser: EventEmitter<number> = new EventEmitter<number>();
}
