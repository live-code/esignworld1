import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ew-list-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <li class="list-group-item">
      {{user.name}}
      <button (click)="deleteUser.emit(user.id)">Delete</button>

      <i class="fa fa-arrow-circle-down" (click)="isOpen = !isOpen"></i>

      <ew-card [twoway]="false" [isOpen]="isOpen" (toggle)="isOpen = !isOpen">
        username is {{user.name}}
      </ew-card>
      
      <ew-tabbar></ew-tabbar>
    </li>
  `,
  styles: [
  ]
})
export class ListItemComponent  {
  @Input() user: any;
  @Output() deleteUser: EventEmitter<number> = new EventEmitter<number>();

  isOpen = false;

}
