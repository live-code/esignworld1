import { ChangeDetectionStrategy, Component, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'ew-user-form',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <button (click)="save.emit(null)">add</button>
    {{update()}}
  `,
})
export class UserFormComponent  {
  @Output() save: EventEmitter<any> = new EventEmitter<number>();

  update() {
    console.log('update')
  }
}
