import { NgModule } from '@angular/core';
import { UsersComponent } from './users.component';
import { UserFormComponent } from './components/user-form.component';
import { ListComponent } from './components/list.component';
import { ListItemComponent } from './components/list-item.component';
import { SharedModule } from '../../shared/shared.module';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    //  features: users
    UsersComponent,
    UserFormComponent,
    ListComponent,
    ListItemComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    RouterModule.forChild([
      { path: '', component: UsersComponent}
    ])
  ]
})
export class UsersModule {

}
