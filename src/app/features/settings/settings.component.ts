import { ChangeDetectionStrategy, Component, HostBinding, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'ew-settings',
  template: `
    <p [ewColor]="value" bg="yellow">
      settings works!
    </p>
    <button [ewUrl]="'http://www.google.com'">google</button>
    
    <div 
      ewXhr="http://localhost:3000/login"
      (load)="load($event)"
    ></div>
    
    <button (click)="value = 'grey'">change color</button>
    
  `,
  styles: [
  ]
})
export class SettingsComponent implements OnInit {
  value = 'purple';
  constructor() { }

  ngOnInit(): void {
  }

  update(): void {
  }

  load(res: Event) {
    console.log(res)
  }
}
