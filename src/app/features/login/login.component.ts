import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'ew-login',
  template: `
    <hr>
    <button routerLink="registration">registration</button>
    <button routerLink="signin">signin</button>
    <button routerLink="lostpass">lostpass</button>
    <hr>
    <router-outlet></router-outlet>
    <hr>
    copyright
  `,
  styles: [
  ]
})
export class LoginComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
