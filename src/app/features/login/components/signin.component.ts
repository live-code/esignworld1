import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../../../core/auth/auth.service';

@Component({
  selector: 'ew-signin',
  template: `
    <h1>Login</h1>
    <input type="text">
    <input type="text">
    <button (click)="signInHandler()">Login</button>
  `,
  styles: [
  ]
})
export class SigninComponent {

  constructor(
    public authService: AuthService,
    private router: Router
  ) {
  }


  signInHandler() {
   this.authService.login()
     .subscribe(
       (res) => {
         console.log(res)
        this.router.navigateByUrl('admin')
       },
       err => console.log('login non valid', err)
     );
    // then() //  nel caso di promise
  }
}
