import { Component, OnInit } from '@angular/core';
import { City, Country, isCountry } from '../../model/country';

@Component({
  selector: 'ew-uikit',
  template: `
    <div class="container mt-5">
      <ew-tabbar
        [items]="countries"
        [active]="activeCountry"
        (tabSelect)="setActiveCountry($event)"
      ></ew-tabbar>

      <ew-tabbar
        [items]="activeCountry.cities"
        [active]="activeCity"
        labelField="name"
        (tabSelect)="setActiveCity($event)"
      ></ew-tabbar>

      <div class="card" *ngIf="activeCity">
        La città è {{activeCity?.name}}
      </div>

      <ew-card
        title="Mario Rossi"
        [marginBottom]="5"
        [headerCls]="counter > 5 ? 'bg-dark text-white' : null"
        headerFontColor="red"
        icon="link"
        (iconClick)="openUrl('http://www.mariorossi.it')"
      >
        <input type="text" >
        <input type="text">
        <input type="text">
        <input type="text">
      </ew-card>

      <pre>{{panelIsOpened ? 'true' : 'false'}}</pre>
      <ew-card
        [title]="'Pippo'"
        [icon]="'bluetooth'"
        [isOpen]="panelIsOpened"
        [twoway]="false"
        (iconClick)="counter = 10"
        (toggle)="panelIsOpened = !panelIsOpened"
      >
        <h1>Google Map</h1>
      </ew-card>

      <ew-card
        title="Mario Rossi"
        [(isOpen)]="panelIsOpened"
        [twoway]="true"
      >
        <h1>Google Map</h1>
      </ew-card>
    </div>
  `,
  styles: [
  ]
})
export class UikitComponent {

  countries: Country[] = [
    {
      id: 1001,
      label: 'UK',
      lat: 42,
      lng: 12,
      cities: [
        { id: 1, name: 'London'},
        { id: 2, name: 'Brighton'},
      ]
    },
    {
      id: 1002,
      label: 'Italy',
      lat: 56,
      lng: 14,
      cities: [
        { id: 3, name: 'Rome'},
        { id: 4, name: 'Milan'},
        { id: 5, name: 'Trieste'},
      ]
    },
    {
      id: 1003,
      label: 'Spain',
      lat: 11,
      lng: 4,
    }
  ];

  activeCountry: Country;
  activeCity: City;
  counter = 0;
  panelIsOpened = false;

  constructor() {
    setTimeout(() => {
      this.panelIsOpened = true;
    }, 1100);
    this.setActiveCountry(this.countries[0])
    this.foo(this.countries[0].cities[0])
  }

  foo(obj: Country | City) {
    if (isCountry(obj)) {
      console.log('sono  una nazione')
    } else {
      console.log('citys')
    }

  }




  openUrl(url: string) {
    window.open(url)
  }


  setActiveCountry(country: Country) {
    this.activeCountry = country;
    if (this.activeCountry.cities) {
      this.activeCity = this.activeCountry.cities[0];
    } else {
      this.activeCity = null;
    }
  }

  setActiveCity(city: City) {
    this.activeCity = city;
  }
}
