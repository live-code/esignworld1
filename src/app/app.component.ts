import { Component } from '@angular/core';

@Component({
  selector: 'ew-root',
  template: `
    <ew-navbar></ew-navbar>
    <hr>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {



}

