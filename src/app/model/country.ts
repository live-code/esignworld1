export interface Country {
  id: number;
  label: string;
  lng: number;
  lat: number;
  cities?: City[];
}
export interface City {
  id: number;
  name: string;
}


export function isCountry(obj: Country | City): obj is Country {
  return (obj as Country).label !== undefined;
}
// type guards

// type predicated
