import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavbarComponent } from './layout/navbar.component';
import { RouterModule } from '@angular/router';
import { AuthInterceptor, DbService, LogService, PendingService, RealLogService } from './auth/auth.interceptor';
import { environment } from '../../environments/environment';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './auth/auth.service';
import { IfloggedDirective } from './auth/iflogged.directive';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [NavbarComponent, IfloggedDirective],
  exports: [
    NavbarComponent,
    IfloggedDirective
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    /*{
      provide: HTTP_INTERCEPTORS,
      useFactory: (authService, pendingService) => {
        return new AuthInterceptor(
          authService,
          pendingService,
          environment.production ? 0 : 500
        );
      },
      multi: true,
      deps: [ AuthService, PendingService ]
    },*/
    {
      provide: LogService,
      useFactory: (dbService) => {
        return environment.production ?
          new RealLogService('easy', dbService) :
          new RealLogService('verbose', dbService);
      },
      deps: [DbService]
    }
  ]
})
export class CoreModule { }
