import { Directive, HostBinding } from '@angular/core';
import { AuthService } from './auth.service';

@Directive({
  selector: '[ewIflogged]'
})
export class IfloggedDirective {
  @HostBinding() class = 'btn btn-primary'
  @HostBinding() get hidden() {
    return !this.authService.isLogged();
  }

  constructor(private authService: AuthService) {
  }
}
