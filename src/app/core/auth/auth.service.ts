import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Auth } from './auth';
import { share, tap } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class AuthService {
  auth: Auth;

  constructor(private http: HttpClient) {
  }

  login(): Observable<Auth> {
    const req$ = this.http.get<Auth>('http://localhost:3000/login')
      .pipe(share());
    req$.subscribe(
      res => {
        console.log('succ')
        this.auth = res
      });
    return req$;

    // with Promise
    /*
    return this.http.get<Auth>('http://localhost:3000/login')
      .toPromise()
      .then(res => this.auth = res)
    */
  }

  logout() {
    this.auth = null;
  }

  isLogged(): boolean {
    return !!this.auth;
  }
}
