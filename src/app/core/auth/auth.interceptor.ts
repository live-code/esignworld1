import { Injectable } from '@angular/core';
import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { AuthService } from './auth.service';
import { catchError, delay, finalize, tap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class PendingService {
  value = false;
}

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private authService: AuthService,
    private pendingService: PendingService,
    // private delayValue = 0,
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // ...
    this.pendingService.value = true;
    let clonedReq = req;

    if (this.authService.isLogged()) {
      clonedReq = req.clone({
        setHeaders: {
          Authorization: 'Bearer ' + this.authService.auth.token
        }
      })
    }

    return next.handle(clonedReq)
      .pipe(
        delay(0 /*this.delayValue*/),
        tap(res => this.pendingService.value = false),
        catchError(err => {
          if (err instanceof HttpErrorResponse) {
            switch (err.status) {
              default:
              case 404:
                console.log('redirect')
                break;
            }
          }
          return of(err)
        })
      )
  }

// ----------o-----o-------o---------o------o------o----
}


/// =====================
// example of useFactory (see core.module.ts)
export class LogService {
  doSomething() {}
}
export class DbService {
  doSomething() {}
}

export class RealLogService {
  constructor(logType: string, private dbService: DbService) {
  }

  doSomething() {
    this.dbService.doSomething();
  }
}
