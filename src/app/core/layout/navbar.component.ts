import { Component, OnInit } from '@angular/core';
import { PendingService } from '../auth/auth.interceptor';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'ew-navbar',
  template: `
    <button [routerLink]="'uikit'">uikit</button>
    <button [routerLink]="'admin'" ewIflogged>admin</button>
    <button [routerLink]="'login'">login</button>
    <button [routerLink]="'settings'">settings</button>
    <button routerLink="users">users</button>
    <button ewIflogged (click)="logout()">logout</button>
    <input type="text" ngModel>
    <i class="fa fa-spinner" *ngIf="pendingService.value"> </i>

  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {

  constructor(
    private router: Router,
    public authService: AuthService, public pendingService: PendingService) { }

  ngOnInit(): void {
  }

  logout() {
    this.router.navigateByUrl('login')
    this.authService.logout();
  }
}
