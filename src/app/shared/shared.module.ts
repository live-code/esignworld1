import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardComponent } from './components/card.component';
import { TabbarComponent } from './components/tabbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ColorDirective } from './directives/color.directive';
import { UrlDirective } from './directives/url.directive';
import { XhrDirective } from './directives/xhr.directive';



@NgModule({
  declarations: [
    CardComponent,
    TabbarComponent,
    ColorDirective,
    UrlDirective,
    XhrDirective,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    CardComponent,
    TabbarComponent,
    ColorDirective,
    UrlDirective,
    XhrDirective
  ]
})
export class SharedModule { }
