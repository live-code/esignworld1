import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ew-card',
  // changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
   
    <div 
      class="card" 
      [ngClass]="'mb-' + marginBottom"
      [ngStyle]="headerStyle"
      [style.color]="headerFontColor"
    >  
      <div
        class="card-header" 
        [ngClass]="headerCls"
        (click)="clickHandler()"
      >
        {{title}}
        
        <i
          *ngIf="icon"
          class="pull-right" 
          [ngClass]="'fa fa-' + icon"
          (click)="iconClick.emit()"
        ></i>
      </div>
      <div class="card-body" *ngIf="isOpen">
        <ng-content></ng-content>
      </div>
    </div>
  `,
})
export class CardComponent {
  @Input() title = 'Widget';
  @Input() marginBottom: 0 | 1 | 2 | 3 | 4 | 5 = 0;
  @Input() headerCls: string;
  @Input() headerStyle: any;
  @Input() headerFontColor: string;
  @Input() icon: string;
  @Input() isOpen = true;
  @Input() twoway = false;

  @Output() iconClick: EventEmitter<void> = new EventEmitter<void>();
  @Output() toggle: EventEmitter<void> = new EventEmitter<void>();
  @Output() isOpenChange: EventEmitter<boolean> = new EventEmitter<boolean>();

  clickHandler() {

    if (this.twoway) {
      this.isOpen = !this.isOpen;
      this.isOpenChange.emit(this.isOpen);
    } else {
      this.toggle.emit()
    }
  }

}
