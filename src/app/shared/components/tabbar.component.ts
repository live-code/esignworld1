import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'ew-tabbar',
  template: `
    <ul class="nav nav-tabs">
      <li *ngFor="let tab of items" class="nav-item"
          (click)="tabClickHandler(tab)">
        <a class="nav-link"
           [ngClass]="{'active': tab.id === active?.id}"
        >
          {{tab[labelField]}}
        </a>
      </li>
    </ul>
  `,
})
export class TabbarComponent<T> {
  @Input() items: T[];
  @Input() active: T;
  @Input() labelField = 'label';
  @Output() tabSelect: EventEmitter<T> = new EventEmitter();

  tabClickHandler(tab: T) {
    this.active = tab;
    this.tabSelect.emit(tab);
  }
}
