import { Directive, EventEmitter, Input, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Directive({
  selector: '[ewXhr]'
})
export class XhrDirective {
  @Output() load: EventEmitter<any> = new EventEmitter<any>();

  @Input() set ewXhr(val: string) {
    this.http.get(val)
      .subscribe(res => this.load.emit(res))
  }

  constructor(private http: HttpClient) { }

}
