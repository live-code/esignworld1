import { Directive, ElementRef, HostBinding, Input, OnChanges, Renderer2, SimpleChanges } from '@angular/core';

@Directive({
  selector: '[ewColor]',
})
export class ColorDirective implements OnChanges {
  @Input() ewColor: string;
  @Input() bg: string
    // this.element.nativeElement.style.color = val;

  ngOnChanges(changes: SimpleChanges): void {
    console.log('change', changes)
    if (changes.ewColor) {
      this.renderer2.setStyle(this.element.nativeElement, 'color', this.ewColor);
    }
    if (changes.bg) {
      this.renderer2.setStyle(this.element.nativeElement, 'backgroundColor', this.bg);
    }
  }


  constructor(
    private element: ElementRef<HTMLElement>,
    private renderer2: Renderer2
  ) {
  }

  /*@Input() set ewColor(val: string) {
    // this.element.nativeElement.style.color = val;
    this.renderer2.setStyle(this.element.nativeElement, 'color', val)
  }

  constructor(
    private element: ElementRef<HTMLElement>,
    private renderer2: Renderer2
  ) {
  }*/

  // solution hostbinding
 /* @Input() ewColor: string;

  @HostBinding('style.color') get color() {
    return this.ewColor
  }
*/
}

