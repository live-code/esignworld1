import { Directive, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[ewUrl]'
})
export class UrlDirective {
  @Input() ewUrl: string;

  @HostListener('click')
  clickHandler() {
    window.open(this.ewUrl)
  }

}


